# coding: utf-8
from aiida.orm import (Node, Calculation, JobCalculation, Code, Data,
    Computer, Group, DataFactory, CalculationFactory)
from aiida.backends.djsite.db import models
import json
import sys, os

def dump(group_name):
    g = Group.get(group_name)

    # Sort time
    times = []
    for n in g.nodes:
        times.append(n.ctime)
    sort_map = [i[0] for i in sorted(enumerate(times), key=lambda x:x[1])]

    # Pritty horrible trick to get the list filled in order of execution time
    results = [None for _ in sort_map]
    for i, n in enumerate(g.nodes):
        c = n.get_code()
        r = n.res
        inp = n.inp.parameters
        # Funny one liner to get a dictionary out of the various properties of r
        res_dict = dict(zip([x for x in dir(r) if x[0]!='_'] ,[getattr(r,x) for x in dir(r) if x[0]!='_']))
        inp_list = [ getattr(n.inp.parameters.dict, x) for x in dir(inp.dict) ]
        results[sort_map.index(i)] = [c.label, c.description, res_dict, inp_list]

    with open (os.path.join('data',group_name+'.json'), 'w') as f:
        json.dump(results, f)

if __name__ == '__main__':
    if len(sys.argv) >= 1:
        for name in sys.argv[1:]:
            dump(name)
    else:
        print("Usage: {} groupname [groupname ...]".format(sys.argv[0]))
